# Do not remove the header from this file or other files created from it
# GPL Version 3
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# Simple program to convert multiline files into singleline file
# Liner 1.0
# KEEP A BACKUP OF A FILE JUST IN CASE!!!!

filename=raw_input('Enter filename to be changed (dont add .*): ')
filetype=raw_input('Enter type of the file in this format .*: ')
f = open(filename+filetype, 'r')
lines = f.readlines()
mystr = ''.join([line for line in lines]) #removes new lines
mystr = mystr.replace('\n', ' ').replace('\r', '')  #replaces windows symbol of end of the line
f.close()
file=open(filename+'_output'+filetype,'w') #creates output file
file.write('%s' %mystr)
file.close()
print ('Done!')