# Do not remove the header from this file or other files created from it
# GPL Version 3
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Use it at your own risk
# ConsoleTyper 1.2

from __future__ import division
from datetime import datetime
import time
import random

'''imported lib used for randomness and time'''
seed=datetime.now()
random.seed(seed.second) #initializing randomness, actually not required as time is already used as a default seed
multiline=False # checks if there will be a follow up call of mode through recursion of moder in modem
First_time=True
modex='' #urs input in main menu

def mode(lstr):
	'''takes in a string which will be promted to be typed up and results compared'''
	count=0 # used to compare lenght of original lst and input
	usr_count=0 # as above
	error=0 #counts errors

	count = len(lstr)
	if multiline==False or First_time==True: #warning will not annoy between lines
		rdy=raw_input ('Enter promted text without pressing enter!!!! Are you ready? y/n: ') #so the person can prepare for what is coming next
	else:
		rdy='y' #sets rdy status as true in follow up rounds of multiline mode
	if rdy!='y': 
		print('Unexpected input, returning to main selection') #is the user trolling or made a mistake?
		return 0
	else:
		time_start=time.time()
		usr_input=raw_input(lstr+'\n')#usr can enter input
		time_finish=time.time()
		timer=time_finish-time_start
		print('Congratulations your time is %f seconds') %(timer) #time it took
		usr_count=len(usr_input) #counting how many characters were entered
		#now checking for errors
		if usr_count==count: #if same amount of letters entered as in original
			for i in range(count): #going through lists and comparing them side by side
				if lstr[i]!=usr_input[i]:
					error+=1 #if difference noted - logged as an error
		#if different
		else:
			print ('Entered data does is not the same length as displayed.')
			print ('usr=%d, original=%d') %(usr_count,count)
			return
		#printing results
		print timer
		print ('%d %% errors' % (error/usr_count*100))
		print ('%d letters per minute' % (usr_count/timer*60))
		if multiline==False: #avoids breaking the cycle during multiline mode (modem)
			usr=raw_input('Press enter to continue')

def moder(char_number):
	'''classic mode where a random set of letters is generated from ascii file
	and player is promted to type them all up'''
	file=open("ascii_symbols_en.txt",'r')
	gen1=file.read() #list which will contain all char starts with a ' '
	gen=''
	for i in range(char_number):
		gen+=random.choice(gen1)
	file.close()
	mode(gen)

def modec(file_name):
	'''uses custom txt file and promts user to type it up'''
	file=open(file_name+'.txt', 'r')
	lst=file.read()
	file.close()
	mode(lst)

def modea(integer):
	'''chooses which of the 4 senteces to be used'''
	lst=['The quick brown fox jumps over the lazy dog.',
		'Pack my box with five dozen liquor jugs.',
		'A quick movement of the enemy will jeopardize six gunboats.',
		'The five boxing wizards jump quickly.'] #sentences with all characters
	gen='' #generated string
	for i in range (integer): #chooses random sentences integer times
		chosen=random.randrange(3)
		gen+=lst[chosen]
	mode(gen)

def modem(line,char):
	'''Mode-Multiline
		Allows more than one line to be provided to the player'''
	global multiline
	global First_time
	multiline=True
	for i in range (line):
		moder(char)
		First_time=False
	multiline=False
	usr=raw_input('Press enter to continue') #as it is disabled in the mode


while modex!='q':
	print('To play random mode enter r, multi line random tpye m, for custom file type c, En letters only type a, to quit type q') #mode selection
	modex=raw_input('Enter selected mode letter: ')
	if modex=='r':
		char=raw_input('How many characters do you want to be used?: ') #number of symbols to be generated
		char=int(char)   
		moder(char)
	elif modex=='c':
		print ("Ensure that the file is a single line as enter cant be used. To convert file run liner.py")
		file=raw_input('Enter filename, do not write .txt: ')
		modec(file)
	elif modex=='a':
		print ('This mode will give you random sentence which has all letters')
		char=raw_input('how many sentences should be used? ')
		char=int(char)
		modea(char)
	elif modex=='m':
		char=raw_input("How many lines? ") #number of times moder is to be called in modem
		char1=raw_input("how many characters per line? ") #number given to moder in modem
		char=int(char)
		char1=int(char1)
		modem(char, char1)